---
title: "A propos de ce site"
output:
  html_document:
    toc: true
    toc_float: true
    number_sections: yes
    theme: flatly
---

# Sitographie {-}

+ [http://larmarange.github.io/analyse-R/](http://larmarange.github.io/analyse-R/) => Très beau site très généraliste et très pédagogique
+ [http://www.sthda.com/french/](http://www.sthda.com/french/) => Site dédié aux analyses factorielles sous R

# Bibliographie {-}

+ Feuillet, T., Cossart, E., & Commenges H. (2019). **_Manuel de géographie quantitative. Concepts, outils, méthodes_**. A. Colin.
+ Fox, J., & Weisberg, S. (2018). **_An R companion to applied regression_**. Sage Publications.
+ Maumy-Bertrand, M., & Bertrand, F. (2018). **_Initiation à la statistique avec R-3e éd.: Cours, exemples, exercices et problèmes corrigés_**. Dunod.

