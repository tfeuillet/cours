---
title: "Statistiques bivariées"
output:
  html_document:
    toc: true
    toc_float: true
    number_sections: yes
    theme: flatly
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Préalablement à l'exercice, chargez les packages, puis ouvrez et préparez la table ainsi :

```{r, eval=TRUE, message=FALSE}

# Chargez les packages
library(readxl)
library(gtools)
library(ggplot2)
library(dplyr)
library(car)

# Importez la table (disponible ici : https://gitlab.huma-num.fr/tfeuillet/cours/data)
df <- read_excel("data/table_climato.xlsx")

# Discrétisez la variable "ampl" en terciles et renommez les niveaux
df$amplDisc <- quantcut(df$ampl, 3) 
levels(df$amplDisc) <- c("faible","moyenne","forte") 

# Discrétise la variable "jrs_neige" en quartiles et renommez les niveaux
df$neigeDisc <- quantcut(df$jrs_neige, 4) 
levels(df$neigeDisc) <- c("faible","moyenne","forte","très forte") 
```
&nbsp;

# Relations entre deux variables quantitatives

## Caractérisation de la relation

On va chercher ici à étudier la relation entre la distance au littoral et l'amplitude thermique. On commence par observer la forme du nuage de points (graphique cartésien) entre ces deux variables, pour juger de la nature, du sens et de l'intensité d'une éventuelle relation.

```{r, out.width = '60%', fig.align = "center"}

plot1 <- ggplot(df, aes(x=dist_litt, y=ampl)) + geom_point() + 
  labs(title="Relation amplitude thermique vs distance au littoral")+ 
  xlab("Distance au littoral (km)") + ylab("Amplitude thermique (degrés)")+
  theme(plot.title = element_text(face = "bold"))
plot1
```

## Coefficient de corrélation linéaire

On constate que la relation est linéaire et positive. On peut donc calculer le **coefficient de corrélation linéaire de Bravais-Pearson** (si la relation n'avait pas été linéaire, le coefficient de Spearman (basé sur les rangs et non sur les valeurs) aurait été préféré) :

```{r}
cor.test(df$ampl, df$dist_litt)
```

Rappelons que comme pour tout test de liaison, l'hypothèse nulle est l'indépendance des variables. Si la *p*-value est > 0.05, on rejette l'hypothèse nulle (il y a donc une relation significative entre les variables).
Ce coefficient est égal à **0.89** et très fortement significatif. Il y a donc bien une forte corrélation positive entre les deux variables.


# Relations entre deux variables qualitatives

L'analyse de la relation entre deux variables qualitatives passe par le tableau de contigence. Le principal test utilisé pour vérifier cette relation est le test du $\chi^2$.  
Nous cherchons ici à tester la relation entre nos deux variables discrétisées (amplitude thermique et jours de neige). Le tableau de contingence était celui-ci :
```{r, eval=TRUE, echo=FALSE}

knitr::kable(table(df$amplDisc, df$neigeDisc))
```
&nbsp;

Comme certains effectifs sont très faibles (voire nuls) dans certaines cases du tableau, il convient de plutôt utiliser le **test exact de Fisher** :

```{r, eval=TRUE}
fisher.test(df$amplDisc, df$neigeDisc)
```
&nbsp;

Juste pour montrer le code, essayons avec le test du $\chi^2$ :
```{r, eval=TRUE}
chi <- chisq.test(df$amplDisc, df$neigeDisc)
chi$p.value
```

Le résultat est très proche.

# Relations entre une variable qualitative et une variable quantitative

Cette approche consiste à vérifier si la moyenne (ou la variance, ou la proportion) d'une variable quantiative varie significativement selon les modalités d'une variable qualitative. Par exemple, est-ce que la température moyenne varie selon l'intensité de l'amplitude thermique (sous forme discrète) ?

```{r, eval=TRUE, out.width = '70%', fig.align = "center"}
boxplot(df$temp ~ df$amplDisc, col="grey50", xlab = "Amplitude thermique", ylab = "Température")
```

## Test de Student

Le test de Student sert à comparer des moyennes dans deux groupes. Il faut que les variances soient identiques.


```{r, eval=FALSE}
t.test(varX~varY, data=data) # test de Student
```

## ANOVA

Pour mettre en relation une variable quantitative avec une variable qualitative à plus de deux modalités, l'ANOVA (analyse de la variance) est adaptée. C'est un type de régression qui permet de voir dans quelle mesure les moyennes varient selon les modalités en questions.

```{r}
anova <- aov(temp ~ amplDisc, data=df)
summary(anova)
```




