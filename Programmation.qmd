---
title: "Initiation à la programmation en analyse spatiale"
author: "Thierry Feuillet"
author-title: "AUTEUR"
editor: visual
lang: fr
format:
  html:
    toc: true
    link-external-icon: true
    link-external-newwindow: true
    embed-resources: true
    code-copy: true
navbar: true
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Cette page a pour but de vous initier à la programmation en géomatique avec R. L'objectif est de réussir à automatiser un script de géotraitements pour plusieurs tailles de mailles au sein d'une grille (objectif très commun en analyse spatiale). Plus précisément, nous allons calculer automatiquement la densité linéaire de cours d'eau dans le Calvados dans des grilles dont les mailles varieront (2000, 3000, 4000 et 5000 m).
Cette page sera l'occasion de présenter simplement deux étapes essentielles à l'automatisation des traitements : les **fonctions** et les **boucles**.

# Chargez les packages

Nous allons charger le package `sf` pour la manipulation des données spatiales, `tmap` pour la cartographie, `dplyr` et `tidyr` pour la manipulation des données, et `purrr` pour les boucles.

```{r, message=FALSE}
library(sf)
library(tmap)
library(dplyr)
library(tidyr)
library(purrr)
```

# Importez les fichiers

Téléchargez les limites du département du Calvados `DEP14.gpkg` ici : [lien](https://gitlab.huma-num.fr/tfeuillet/cours/-/blob/main/data/DEP14.gpkg), et
les cours d'eau `HYDRO14.gpkg` ici : [lien](https://gitlab.huma-num.fr/tfeuillet/cours/-/blob/main/data/HYDRO14.gpkg)
déposez-la dans le `data` de votre projet.

```{r, message=FALSE}
dep14 <- read_sf("data/DEP14.gpkg")
hydro14 <- read_sf("data/HYDRO14.gpkg")
```

# Les fonctions

Première chose à connaitre : les fonctions. Dans nos analyses, nous sommes très vite amenés à produire des fonctions maison, car celles existantes ne répondent pas précisément à nos besoins. Le principe est simple, voilà la syntaxe :


```{r, message=FALSE, eval=FALSE}
function(premierArgument, deuxiemeArgument){
    resultat <- un_script_qui(premierArgument) %>%
                            utilise_les_args(deuxièmeArgument)
    return(resultat)
 }
```

Prenons un exemple simple : il n'existe pas de fonction en R base pour calculer le coefficient de variation d'une variable (qui est égal à l'écart-type divisé la moyenne). On souhaite donc créer une fonction qui le calculerait :


```{r, message=FALSE}
coefVar <- function(x){ 
  cv <- sd(x) / mean(x)
  return(cv)
}
```



Dans cette nouvelle fonction, l'argument `x` correspondra à la variable en question. La fonction `return` permet de préciser que la fonction renverra comme résultat.

Maintenant, appliquons cette fonction :

```{r, message=FALSE}
data("iris")
coefVar(iris$Sepal.Length)
```

# Les boucles

Une boucle consiste à répéter automatiquement un bloc de code, selon des critères bien définis. C'est par exemple le cas si on souhaite calculer notre coefficient de variation sur toutes les variables quantiatives d'une table, ou sur plusieurs tables. Les boucles sont très vite indispensables quand on réalise des géotraitements.
Il existe deux principales façons de faire des boucles en R : avec `for` et avec la famille de fonctions dérivées d'`apply`. En R, il est plutôt déconseillé d'utiliser la boucle `for`, plus gourmande en ressources que la boucle `apply` (cela dit, voir [ici](https://kbroman.wordpress.com/2013/04/02/apply-vs-for/)...).


## Les fonctions `apply`

Les fonctions `apply` permettent de faire des boucles de façon très simplifiée et efficace, sur différents types d'objets :

- `apply()` : répète une fonction sur des vecteurs classiques
- `lapply()` : répète une fonction sur les éléments d'une liste et renvoie une liste
- `sapply()` : répète une fonction sur les éléments d'une liste et renvoie un vecteur
- `mapply()` : répète une fonction sur les premiers éléments de chaque argument
- `tapply()` : répète une fonction sur des sous-ensembles d'un vecteur (fait essentiellement la même chose que la paire `group_by()` / `summarize()` de `dplyr`)

Par exemple, calculons le coefficient de variation sur les quatre premières variables de la table `iris` :

```{r, message=FALSE}
apply(X = iris[,1:4], MARGIN = 2, FUN = coefVar) 
# margin = 2 signifie : sur les colonnes (1 pour les lignes)
```

## Les fonctions `map` de `purrr`

Si les fonctions `apply` ringardisent les boucles `for`, les fonctions `map` du package `purrr` (du tidyverse) font encore mieux ! Jamais boucler n'a été aussi agréable.

- `map()` : répète une fonction sur des vecteurs ou des listes et renvoie une liste
- `map_dbl()` : répète une fonction sur des vecteurs ou des listes et renvoie un vecteur de valeurs (`map_chr()` renvoie des caractères, `map_int()` des entiers, etc.)

Ainsi, répéter notre exemple ci-dessus est d'une simplicité enfantine :

```{r, message=FALSE}
map_dbl(iris[,1:4], coefVar) 
```


# Application géomatique 1 : densité linéaire de cours d'eau

Nous allons maitneant appliquer ces éléments à notre objectif initial de géotraitements, qui consiste à calculer la densité linéaire de cours d'eau à l'échelle du Calvados dans des mailles de différentes dimensions. 
Voilà une façon possible de procéder. 

## Import des données

```{r, message=FALSE}
dep14 <- read_sf("data/DEP14.gpkg")
hydro14 <- read_sf("data/HYDRO14.gpkg")
```

## Définition des tailles de maille

Puis nous allons définir les tailles de mailles sur lesquelles nous ferons la boucle :

```{r, message=FALSE}
# Liste des tailles de maille
cell_sizes <- c(2000, 3000, 4000, 5000) # en m, si CRS métrique
```

## Définition de la fonction de géotraitements

Il faut ensuite définir la fonction qui permet de calculer la densité linéaire de cours d'eau. Nous allons appeler cette fonction `densLin` :

```{r, message=FALSE}
densLin <- function(emprise, cellsize, obsLin) {
  # Création de la grille
  grid <- st_make_grid(x = emprise, cellsize = cellsize) %>% 
    st_as_sf() %>% 
    st_intersection(emprise)  # Croise avec l'emprise pour limiter à la zone utile
  
  # Ajout d'un ID unique et calcul de la surface des cellules
  grid <- grid %>% 
    mutate(ID_grid = 1:nrow(grid), surf = as.numeric(st_area(grid)))  # Surface en m²
  
  # Intersection avec les objets linéaires
  tmp <- st_intersection(grid, obsLin)
  tmp <- tmp %>% mutate(length = as.numeric(st_length(tmp)))  # Longueur en m
  
  # Agrégation des longueurs et calcul de la densité
  tmp2 <- tmp %>% 
    group_by(ID_grid) %>% 
    summarise(
      lengthTot = sum(length),
      surf = first(surf),  # Récupère la surface de la cellule
      .groups = "drop"
    ) %>% 
    mutate(densLin = lengthTot / surf * 10^6) %>% # Densité en m/km²  
    st_drop_geometry() # Il faut enlever la géométrie pour permettre le left_join ci-dessous
  
  # Jointure avec la grille initiale
  grid <- grid %>% 
    left_join(tmp2, by = "ID_grid") %>% 
    replace_na(list(lengthTot = 0, densLin = 0)) # Jointure par ID_grid
  
  # Ajouter la taille de la maille
  grid$cellsize <- cellsize
  
  return(grid)  # Retourne la grille enrichie
}
```

## Répétition de la fonction sur tous les maillages

Le code ci-dessus fait la boucle avec `purrr::map()`, puis nomme les résultats (stockés dans `results`) en fonction de la taille de la maille :

```{r, message=FALSE, warning=FALSE}
# Appliquer la fonction densLin sur chaque taille de maille
results <- map(
  cell_sizes,
  ~ densLin(emprise = dep14, cellsize = ., obsLin = hydro14)
)

# Nommer les résultats pour les identifier facilement
names(results) <- paste0("grid_", cell_sizes, "m")
```


## Cartographie des résultats

On veut appliquer une fonction de cartographie sur quatre objects, si bien qu'il faut à nouveau faire une boucle :

```{r, message=FALSE}
tmaps <- map(
  results,
  ~ tm_shape(.x) +
    tm_basemap("OpenTopoMap") +
    tm_polygons(
      col = "densLin",
      style = "quantile",
      n = 6,
      palette = "YlGnBu",
      title = "Densité (m/km²)"
    ) +
    tm_layout(title = paste0("Maille : ", .x$cellsize, " m"))
)
```


Puis il ne reste qu'à afficher les cartes sur une planche avec la fonction `tmap_arrange` de `tmap` :

```{r, message=FALSE}
tmap_mode("view")
tmap_arrange(tmaps[[1]], tmaps[[2]], tmaps[[3]], tmaps[[4]])
```


Et le tour est joué ! 


# Application géomatique 2 : calcul d'un indice de diversité lithologique



## Défintion de la fonction de géotraitements

Il est tout à fait possible d'adapter cette fonction à des polygones et non des lignes, par exemple pour travailler sur la diversité de l'occupation du sol (part de chaque mode d'occupation dans chaque maille, pour chaque résolution). Nous développons ci-dessous une fonction permettant de calculer la diversité lithologique dans nos mailles, sous la forme d'un indice de Shannon (qui va donc quantifier la diversité des affleurements dans chaque maille).

Nous partons de la couche géologique de la BD CHARM (disponible [ici](https://gitlab.huma-num.fr/tfeuillet/cours/-/blob/main/data/geol_14.gpkg) pour le Calvados)

```{r, message=FALSE}
litho <- read_sf("data/geol_14.gpkg")
```


Créons maintenant la fonction de calcul de l'indice de diversité  `divLitho`: 

```{r, message=FALSE}
divLitho <- function(emprise, cellsize, litho) {
  # Vérification des inputs
  stopifnot(inherits(emprise, "sf"), inherits(litho, "sf"))
  
  # Création de la grille
  grid <- st_make_grid(x = emprise, cellsize = cellsize) %>% 
    st_as_sf() %>% 
    st_intersection(emprise) %>% 
    mutate(ID = row_number(), surf = as.numeric(st_area(.)))  # ID unique et surface
  
  # Intersection avec les polygones litho
  litho_grid <- st_intersection(litho, grid) %>%
    mutate(surface_intersection = as.numeric(st_area(.)))  # Surface après intersection
  
  # Agrégation par maille et lithologie
  part_surface_litho <- litho_grid %>%
    st_drop_geometry() %>%
    group_by(ID, CODE) %>% # CODE = ID litho
    summarise(surface_litho = sum(surface_intersection), .groups = "drop")
  
  # Calcul des pourcentages par maille
  part_litho <- part_surface_litho %>%
    group_by(ID) %>%
    mutate(surface_totale = sum(surface_litho),
           part_litho = (surface_litho / surface_totale) * 100) %>%
    ungroup() %>%
    select(ID, CODE, part_litho) %>%
    pivot_wider(names_from = CODE, values_from = part_litho, values_fill = 0)
  
  # Calcul de l'indice de Shannon
  library(vegan)
  part_litho$shannon <- diversity(part_litho %>% select(-ID), index = "shannon")
  
  # Jointure des résultats avec la grille
  grid <- grid %>% left_join(part_litho, by = "ID") %>% na.omit()
  
  return(grid)
}

```

## Répétition de la fonction sur tous les maillages

Le code ci-dessus fait la boucle avec `purrr::map()`, puis nomme les résultats (stockés dans `results`) en fonction de la taille de la maille. Etant donné que la géocomputation est ici assez lourde, on peut paralléliser les calculs avec le package `furrr` :

```{r, message=FALSE, warning=FALSE}
library(furrr)
plan(multisession)  # Plan de parallélisation

resultsLitho <- future_map(
  cell_sizes,
  ~ divLitho(emprise = dep14, cellsize = ., litho = litho),
  .progress = TRUE
)
names(resultsLitho) <- paste0("grid_", cell_sizes, "m")

```

## Cartographie des résultats

Comme pour la desnité linéaire de cours d'eau, on peut appliquer une fonction de cartographie sur les quatre objects :


```{r, message=FALSE}
tmaps <- map(
  resultsLitho,
  ~ tm_shape(.x) +
    tm_basemap("OpenTopoMap") +
    tm_polygons(
      col = "shannon",
      style = "quantile",
      n = 6,
      palette = "viridis",
      title = paste("Diversité lithologique (maille:", unique(.x$cellsize), "m)")
    ) +
    tm_layout(legend.outside = TRUE)
)

```


Puis il ne reste qu'à afficher les cartes sur une planche avec la fonction `tmap_arrange` de `tmap` :

```{r, message=FALSE}
tmap_mode("view")
tmap_arrange(tmaps[[1]], tmaps[[2]], tmaps[[3]], tmaps[[4]])
```


